# [mtarld/symbok-bundle](https://phppackages.org/p/mtarld/symbok-bundle)

Runtime code generator

[![PHPPackages Rank](http://phppackages.org/p/mtarld/symbok-bundle/badge/rank.svg)](http://phppackages.org/p/mtarld/symbok-bundle)
[![PHPPackages Referenced By](http://phppackages.org/p/mtarld/symbok-bundle/badge/referenced-by.svg)](http://phppackages.org/p/mtarld/symbok-bundle)

* [*Say good bye to endless Symfony classes!*](https://medium.com/@mathias.arlaud/symbok-dbc77f1d8ff8)
* https://phppackages.org/s/plumbok
* https://projectlombok.org/